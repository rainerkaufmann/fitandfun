import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';

export default class LoginView extends Component {
  render() {
    return (
      <Image
        source={require('../FitAndFunLogin.jpg')}
        style={styles.container}>
      <Text style={styles.asking}>
        DONT HAVE AN ACCOUNT?
      </Text>
      <Text style={styles.signup}>
      SIGN UP
      </Text>
      </Image>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  asking: {
    fontSize: 16,
    textAlign: 'center',
    margin: 10,
    marginTop: 470,
    color: '#FFFFFF',
    fontWeight: 'bold',
    backgroundColor: 'transparent'
  },
  signup: {
    textAlign: 'center',
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 10,
    marginBottom: 5,
    backgroundColor: 'transparent'
  },
});
